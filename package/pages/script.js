/*@#common-include#(constants.js)*/
/*@#include#(canvasUtil.js)*/

// canvas and context are global variables
let canvas = document.getElementById(questionLocalID + '_canvas');
let ctx = canvas.getContext('2d');
// set width/height values of canvas through constants.js
canvas.width = WIDTH;
canvas.height = HEIGHT;

// HTML elements used, A/B/C are triangle points, X/Y are question points,
// X_ANS/Y_ANS are html elements used for answering (i.e. forms) 
const POINT = {
  A: document.getElementById(`${questionLocalID}_A`),
  B: document.getElementById(`${questionLocalID}_B`),
  C: document.getElementById(`${questionLocalID}_C`),

  X: document.getElementById(`${questionLocalID}_X`),
  Y: document.getElementById(`${questionLocalID}_Y`),
  X_ANS: document.getElementById(`${questionLocalID}_ans_X`),
  Y_ANS: document.getElementById(`${questionLocalID}_ans_Y`)
}

// Connection between backend names and frontend names (hacky)
const POINT_NAME_MAP = {
  'x': POINT.X_ANS,
  'y': POINT.Y_ANS
}

var api = {
  /**
   * Called during question initialization. This is called twice when the question is completed (once
   * with user-values and once with correct-values).
   */
  initQuestion: function (cst, st) {
    setPointHTML(POINT.A, cst.point.a.coords, cst.point.a.color);
    setPointHTML(POINT.B, cst.point.b.coords, cst.point.b.color);
    setPointHTML(POINT.C, cst.point.c.coords, cst.point.c.color);

    setAnswerPointHTML(POINT.X, POINT.X_ANS, cst.qPoint.x.coords, st.qs.x.color);
    setAnswerPointHTML(POINT.Y, POINT.Y_ANS, cst.qPoint.y.coords, st.qs.y.color);

    initQuestion(cst, st);
  },

  /**
   * Gets the current state from the task (i.e. x and y rgb values).
   */
  takeState: function () {
    return {
      x: {
        color: getColorFromForm(POINT.X_ANS)
      },
      y: {
        color: getColorFromForm(POINT.Y_ANS)
      }
    };
  },

  /**
   * Reset state. Called when the user presses the 'reset' button.
   */
  resetState: function (cst, st) {
    setAnswerPointHTML(POINT.X, POINT.X_ANS, cst.qPoint.x.coords, ['', '', '']);
    setAnswerPointHTML(POINT.Y, POINT.Y_ANS, cst.qPoint.y.coords, ['', '', '']);

    initQuestion(cst, st);
  },

  /**
   * Revers state to what it was when the page was loaded. Called when the user presses the
   * 'revert' button.
   */
  revertState: function (cst, st) {
    //TODO: check if reverting works
    setAnswerPointHTML(POINT.X, POINT.X_ANS, cst.qPoint.x.coords, cst.qPoint.x.color);
    setAnswerPointHTML(POINT.Y, POINT.Y_ANS, cst.qPoint.y.coords, cst.qPoint.y.color);

    initQuestion(cst, st);
  }
};

return api;

/**
 * Returns an array of values from the given HTML form.
 * @param {HTMLElement} point html element of the point
 * @returns {array} array of color values that the user entered
 */
function getColorFromForm(point) {
  let color = [];
  let values = point.querySelectorAll('input');
  for (let c of values) {
    color.push(c.value);
  }
  return color;
}

/**
 * Add listener to answer point selected by name that will fire whenever the value changes.
 * @param {HTMLElement} point html element of the point
 * @param {array} coords coordinates of the point 
 * @param {array} color color of the point
 */
function addColorListener(point, name, coords, color) {
  point.addEventListener('input', (e) => {
    let children = point.querySelectorAll('input');
    // Dodgy way of finding out which RGB element is being changed
    let index = Array.prototype.indexOf.call(children, e.target);

    // Select color component based on its index in the form
    color[index] = e.target.value;
    // Draw the point again to refresh color
    drawPoint(ctx, coords, color, name);
  });
}

/**
 * Called to draw the question and add listeners to HTML answer elements.
 * @param {*} cst current state
 * @param {*} st user state
 */
function initQuestion(cst, st) {
  drawQuestion(cst, st);

  // Add listeners to HTML answer elements
  for (let entry of Object.entries(st.qs)) {
    let name = entry[0];
    let color = entry[1].color;
    let coords = cst.qPoint[name].coords;

    addColorListener(POINT_NAME_MAP[name], name, coords, color);
  }
}

/**
 * Draws the entire grid (squares, triangle points and answer points). This is to be called only once.
 * @param {*} ctx 2d context
 * @param {*} cst current state
 * @param {*} st user state
 */
function drawQuestion(cst, st) {
  ctx.clearRect(0, 0, WIDTH, HEIGHT);

  // Draw grid
  for (let i = 0; i < WIDTH / UNIT; i++) {
    for (let j = 0; j < HEIGHT / UNIT; j++) {
      rect(ctx, i * UNIT, j * UNIT, UNIT, UNIT, LINE_COLOR_GRID);
    }
  }

  // Draw triangle points
  for (let entry of Object.entries(cst.point)) {
    let coords = entry[1].coords;
    let color = entry[1].color;
    let x = coords[0], y = coords[1];

    drawPoint(ctx, coords, color, entry[0]);
  }

  // Draw answer points
  for (let entry of Object.entries(st.qs)) {
    let name = entry[0];
    let color = entry[1].color;
    let coords = cst.qPoint[name].coords;

    drawPoint(ctx, coords, color, name.toUpperCase());
  }
}

/**
 * Sets the HTML values of the HTML element point to the given values (coords and color).
 * This is used to set the values for the points A, B, C (i.e. triangle points) - their coordinates
 * and their color intensities.
 * @param {HTMLElement} point html element of point
 * @param {array} coords coordinates of point
 * @param {array} color color values of point
 */
function setPointHTML(point, coords, color) {
  let elements = point.querySelectorAll('span');
  let coordinatesHTML = elements[0], colorHTML = elements[1];
  coordinatesHTML.innerHTML = coords;
  colorHTML.innerHTML = color;
}

/**
 * Sets the HTML value of the HTML element point to the given values (coords and color).
 * This is used to set the values for the points X, Y (i.e. answer points) - their coordinates
 * and their color intensities
 * @param {HTMLElement} point html element of point
 * @param {HTMLElement} pointAns html element of answer point
 * @param {array} coords coordinates of point
 * @param {array} color color of point
 */
function setAnswerPointHTML(point, pointAns, coords, color) {
  point.innerHTML = coords;

  for (let i = 0; i < pointAns.elements.length; i++) {
    let pa = pointAns.elements[i];
    pa.value = color[i];
    // Set readonly value as well so it can't be edited during readonly mode
    pa.readOnly = readOnly;
  }
}
