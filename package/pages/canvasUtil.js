/**
 * Draws a triangle point in the shape of a rectangle with the given name written inside.
 * @param {CanvasRenderingContext2D} ctx context
 * @param {Array} coords x and y coordinates
 * @param {Array} color form of [R, G, B]
 * @param {String} name name of the point which will be written inside
 */
function drawPoint(ctx, coords, color, name = '') {
    // If encountering invalid color, draw white
    for (let c of color) {
        if (Number.isNaN(c) || c === null || c === '') {
            color = [255, 255, 255];
        }
    }

    let x = coords[0], y = coords[1];
    rect(ctx, x * UNIT + 1, y * UNIT + 1, UNIT - 2, UNIT - 2, LINE_COLOR_T);
    fillRect(ctx, x * UNIT + 1, y * UNIT + 1, UNIT - 2, UNIT - 2, `rgb(${color.join(',')})`);
    text(ctx, name.toUpperCase(), x, y, 0.75);
}

/**
 * Draws a rectangle at the given position with the given dimensions and color.
 * @param {CanvasRenderingContext2D} ctx context
 * @param {Number} x x position
 * @param {Number} y y position
 * @param {Number} w width
 * @param {Number} h height
 * @param {String} color valid canvas color
 */
function rect(ctx, x, y, w, h, color = "black") {
    ctx.save();

    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.rect(x, y, w, h);
    ctx.stroke();

    ctx.restore();
}

/**
 * Fills a rectangle at the given position with the given dimensions and color.
 * @param {CanvasRenderingContext2D} ctx context
 * @param {Number} x x position
 * @param {Number} y y position
 * @param {Number} w width
 * @param {Number} h height
 * @param {String} color valid canvas color
 */
function fillRect(ctx, x, y, w, h, color = "black") {
    ctx.save();

    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);

    ctx.restore();
}

/**
 * Draws the given text at the specified position with the given parameters.
 * @param {CanvasRenderingContext2D} ctx context
 * @param {String} text text to write
 * @param {Number} x x position
 * @param {Number} y y position
 * @param {Number} size size of the text
 * @param {String} color valid canvas color
 * @param {String} font font of the text
 */
function text(ctx, text, x, y, size, color = '#000', font = 'arial') {
    ctx.save();

    ctx.font = `${size * UNIT}px ${font}`;
    ctx.lineJoin = 'round';
    ctx.lineWidth = 3;
    ctx.strokeStyle = color;
    ctx.textBaseline="middle"; 
    ctx.textAlign = "center";
    ctx.strokeText(text, x * UNIT + UNIT / 2, y * UNIT + UNIT / 2);

    ctx.font = `${size * UNIT}px ${font}`;
    ctx.fillStyle = "#FFF";
    ctx.fillText(text, x * UNIT + UNIT / 2, y * UNIT + UNIT / 2);

    ctx.restore();
}