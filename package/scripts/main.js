/*@#common-include#(constants.js)*/
/*@#include#(barycentricUtil.js)*/

function questionInitialize(questionConfig) {
  // a, b, c are points of the triangle
  let aRange = questionConfig.aRange, bRange = questionConfig.bRange, cRange = questionConfig.cRange;
  let a = genCoordinates(aRange[0], aRange[1], aRange[2], aRange[3]),
    b = genCoordinates(bRange[0], bRange[1], bRange[2], bRange[3]),
    c = genCoordinates(cRange[0], cRange[1], cRange[2], cRange[3]);

  // x, y are points inside the triangle
  let x = genInsideTriangle(a, b, c),
    y = genInsideTriangle(a, b, c);

  // This is to avoid answer points being the same
  let timeoutCnt = 0, timeoutMax = 10;
  while (arrayEquals(x, y) && timeoutMax > timeoutCnt++) {
    y = genInsideTriangle(a, b, c);
  }

  let colorVar = questionConfig.colorVariation;
  userData.question = {
    // triangle points
    point: {
      a: {
        coords: a, 
        color: [genNumber(255 - colorVar, 255), genNumber(0, colorVar), genNumber(0, colorVar)]
      },
      b: {
        coords: b, 
        color: [genNumber(0, colorVar), genNumber(255 - colorVar, 255), genNumber(0, colorVar)]
      },
      c: {
        coords: c, 
        color: [genNumber(0, colorVar), genNumber(0, colorVar), genNumber(255 - colorVar, 255)]
      }
    },
    // points inside the triangle (answer points)
    qPoint: {
      x: {
        coords: x,
        color: ['','',''],
      },
      y: {
        coords: y,
        color: ['','',''],
      }
    }
  }

  // barycentric coordinates of x and y
  let tx = calculateBarycentric(a, b, c, x);
  let ty = calculateBarycentric(a, b, c, y);

  let points = userData.question.point;
  userData.question.correct = {
    x: {
      color: [
        calculateColor(tx, [points.a.color[0], points.b.color[0], points.c.color[0]]),
        calculateColor(tx, [points.a.color[1], points.b.color[1], points.c.color[1]]),
        calculateColor(tx, [points.a.color[2], points.b.color[2], points.c.color[2]])
      ]
    },
    y: {
      color: [
        calculateColor(ty, [points.a.color[0], points.b.color[0], points.c.color[0]]),
        calculateColor(ty, [points.a.color[1], points.b.color[1], points.c.color[1]]),
        calculateColor(ty, [points.a.color[2], points.b.color[2], points.c.color[2]])
      ]
    }
  };

  // Initial state
  userData.questionState = {
    x: {
      color: ['','','']
    },
    y: {
      color: ['','','']
    }
  }
}


// Ova je metoda namijenjena generiranju varijabli koje se dinamički računaju i nigdje ne pohranjuju.
// Ove varijable bit će vidljive kodu koji HTML-predloške odnosno ServerSide-JavaScript-predloške
// lokalizira.
function getComputedProperties() {
  return { computedProperty: userData.question.answer };
}

// Ova metoda poziva se kako bi se obavilo vrednovanje zadatka. Pozivatelju treba vratiti objekt:
// {correctness: X, solved: Y}, gdje su X in [0,1], Y in {true,false}.
function questionEvaluate() {
  var res = { correctness: 0.0, solved: false };
  userData.correctQuestionState = userData.question.correct;

  let userX = userData.questionState.x,
    userY = userData.questionState.y;

  // solved stays false if question hasnt been edited (all colors are '')
  // return immediately if unanswered
  if (isUnanswered(userX.color) && isUnanswered(userY.color)) {
    return res;
  }

  let correctX = userData.question.correct.x,
    correctY = userData.question.correct.y;

  res.solved = true;
  let correctnessX = getCorrectness(userX, correctX);
  let correctnessY = getCorrectness(userY, correctY);
  res.correctness = Math.round((correctnessX + correctnessY) / 2 * 1E3) / 1E3;

  return res;
}

//TODO: this is really ugly
/**
 * Checks if the specific point is unanswered. Returns true only if all 3 rgb values are empty.
 * @param {array} answer user-entered values
 */
function isUnanswered(answer) {
  return arrayEquals(answer, ['', '', '']);
}

/**
 * Calculates the "correctness" of the user-entered input compared to the correct one.
 * Tolerance is +/-1 from the correct solution (per point component).
 * @param {array} point values that the user entered
 * @param {array} correctPoint correct values
 * @returns {number} correctness value (correct components / 3)
 */
function getCorrectness(point, correctPoint) {
  let total = 0;

  for (let i = 0; i < point.color.length; i++) {
    total += (Math.abs(point.color[i] - correctPoint.color[i]) <= 1)
  }

  return total / 3;
}

// Metoda koja se poziva kako bi izvadila zajednički dio stanja zadatka koje je potrebno za prikaz zadatka.
function exportCommonState() {
  return {
    point: userData.question.point,
    qPoint: userData.question.qPoint
  };
}

// Metoda koja se poziva kako bi izvadilo stanje zadatka koje je postavio korisnik (njegovo rješenje).
// Razlikuje se svrha prikaza: korisnikovo rješenje ili točno rješenje
function exportUserState(stateType) {
  if (stateType == "USER") {
    return { qs: userData.questionState };
  }
  if (stateType == "CORRECT") {
    return { qs: userData.correctQuestionState };
  }
  return {};
}

// Metoda koja se poziva kako bi vratila stanje zadatka koje odgovara nerješavanom zadatku.
function exportEmptyState() {
  return { qs: userData.questionState };
}

// Metoda koja se poziva kako bi pohranila stanje zadatka na temelju onoga što je korisnik napravio.
function importQuestionState(data) {
  userData.questionState = data;
}
