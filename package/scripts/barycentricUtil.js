/**
 * Generates a number in the given range (inclusive).
 * @param {Number} start start of the generation range
 * @param {Number} end end of the range
 * @returns {Number} generated floating point number
 */
function genNumber(start, end) {
    return Math.round(start + Math.random() * (end - start));
}

/**
 * Generates a random integer coordinate for the given range (inclusive).
 * @param {Number} x1 start of the range for x
 * @param {Number} x2 end of the range for x
 * @param {Number} y1 start of the range for y
 * @param {Number} y2 end of the range for y
 * @returns {Array} generated coordinates
 */
function genCoordinates(x1, x2, y1, y2) {
    let x = Math.floor(x1 * UNIT_WIDTH + Math.random() * (x2 - x1) * UNIT_WIDTH);
    let y = Math.floor(y1 * UNIT_HEIGHT + Math.random() * (y2 - y1) * UNIT_HEIGHT);
    return [x, y];
}

/**
 * Calculates coordinates of a random point inside of the triangle specified by x, y and z. The coordinate
 * generated will not be equal to any of the 3 points and it won't be on the edge of the triangle.
 * @param {Array} x 1st point of the triangle
 * @param {Array} y 2nd point of the triangle
 * @param {Array} z 3rd point of the triangle
 * @returns {Array} generated coordinates
 */
function genInsideTriangle(x, y, z) {
    // The range is smaller so that it generates points further inside the triangle
    let r1 = Math.random() * 0.8 + 0.1, r2 = Math.random() * 0.8 + 0.1;
    // This already calculates the barycentric coordinates, but we recalculate later anyway because 
    // it would be ugly to return them here
    let tx = (1 - Math.sqrt(r1)) * x[0] + (Math.sqrt(r1) * (1 - r2)) * y[0] + (Math.sqrt(r1) * r2) * z[0];
    let ty = (1 - Math.sqrt(r1)) * x[1] + (Math.sqrt(r1) * (1 - r2)) * y[1] + (Math.sqrt(r1) * r2) * z[1];
    return [Math.floor(tx), Math.floor(ty)];
}

/**
 * Compares two arrays.
 * @param {Array} a first array
 * @param {Array} b second array
 * @returns {Boolean} true if equal, false otherwise
 */
function arrayEquals(a, b) {
    if (a.length !== b.length) {
        return false;
    }

    for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) {
            return false;
        }
    }

    return true;
}

/**
 * Calculates the area of the triangle specified by the coordinates
 * @param {Array} a 1st point of triangle
 * @param {Array} b 2nd point of triangle
 * @param {Array} c 3rd point of triangle
 * @returns {Number} area of triangle
 */
function calculateTriangleArea(a, b, c) {
    return Math.abs(a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1])) / 2;
}

/**
 * Calculates the barycentric coordinates of the given point t inside the triangle specified by x, y, z.
 * @param {Array} a 1st point of triangle
 * @param {Array} b 2nd point of triangle
 * @param {Array} c 3rd point of triangle
 * @param {Array} t point inside triangle whose barycentric coordinates are being calculated
 * @returns {Array} barycentric coordinates of the point
 */
function calculateBarycentric(a, b, c, t) {
    let barycentric = [];
    let pABC = calculateTriangleArea(a, b, c);
    let pTBC = calculateTriangleArea(t, b, c);
    let pTAC = calculateTriangleArea(t, a, c);
    let pTAB = calculateTriangleArea(t, a, b);
    barycentric[0] = pTBC / pABC;
    barycentric[1] = pTAC / pABC;
    barycentric[2] = pTAB / pABC;
    return barycentric;
}

/**
 * Calculates value of certain color component using the given barycentric coordinates. The parameter c represents the same 
 * color component of 3 different triangle points, for example [R1, R2, R3] where Rx is the red component of the point X.
 * @param {Array} t barycentric coordinates
 * @param {Array} c values of certain color (for example 3 points' red values could be [255, 200, 100])
 * @returns {Number} calculated color component
 */
function calculateColor(t, c) {
    return Math.round(t[0] * c[0] + t[1] * c[1] + t[2] * c[2]);
}